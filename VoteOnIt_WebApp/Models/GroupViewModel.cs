﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VoteOnIt_WebApp.Models
{
    public class CreateGroupViewModel
    {
        [Required]
		[Display(Name = "Group Name")]
        public String GroupName { get; set; }
        [Required]
		[Display(Name = "Private Group")]
        public bool? IsPrivate { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The Invitation Code must be at least 6 characters long.", MinimumLength = 6)]
        [Display(Name = "Invitation Code")]
        public String Code { get; set; }
    }

    public class EditGroupViewModel
    {
		[Display(Name = "Group Name")]
        public String GroupName { get; set; }
		[Display(Name = "Private Group")]
        public bool? IsPrivate { get; set; }
		[Display(Name = "Invitation Code")]
        public String Code { get; set; }
    }
    public class DeleteGroupViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string CurrentPassword { get; set; }
    }
    public class JoinGroupViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The Invitation Code must be at least 6 characters long.", MinimumLength = 6)]
        [Display(Name = "Invitation Code")]
        public String Code { get; set; }
    }
    public class LeaveGroupViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string CurrentPassword { get; set; }
    }
}