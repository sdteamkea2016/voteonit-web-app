﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoteOnIt_WebApp.Models
{
    public class User
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public int LoginAttempts { get; set; }
        public List<Address> Addresses { get; set; }
        public List<Role> Roles { get; set; }
        //public List<MongoDBRef> Groups { get; set; }
    }
}