﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VoteOnIt_WebApp.Models
{

    public class SurveyViewModel
    {
        public String Id { get; set; }
        public String CreatedById { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public bool IsPrivate { get; set; }
        public List<string> Tags { get; set; }
        public List<Comment> Comments { get; set; }
        public List<Question> Questions { get; set; }
        public List<Response> Responses { get; set; }
        public List<Option> Options { get; internal set; }
    }

    public class CreateSurveyViewModel
    {
        public String Title { get; set; }
        [DataType(DataType.MultilineText)]
        public String Description { get; set; }
        [DataType(DataType.DateTime)]
        [Display(Name = "Valid From")]
        public DateTime ValidFrom { get; set; }
        [DataType(DataType.DateTime)]
        [Display(Name = "Valid To")]
        public DateTime ValidTo { get; set; }
        [Display(Name = "Private Survey")]
        public bool IsPrivate { get; set; }
        public List<string> Tags { get; set; }
        public List<Question> Questions { get; set; }
    }

    public class DeleteSurveyViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string CurrentPassword { get; set; }
    }

    public class EditSurveyViewModel
    {
        public String Title { get; set; }
        [DataType(DataType.MultilineText)]
        public String Description { get; set; }
        [DataType(DataType.DateTime)]
        [Display(Name = "Valid From")]
        public DateTime ValidFrom { get; set; }
        [DataType(DataType.DateTime)]
        [Display(Name = "Valid To")]
        public DateTime ValidTo { get; set; }
        [Display(Name = "Is Private")]
        public bool IsPrivate { get; set; }
        //public List<string> Tags { get; set; }
        public List<Question> Questions { get; set; }
    }

    public class Comment
    {

    }

    public class StartSurveyViewModel
    {
        public String Id { get; set; }
        public String CreatedById { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }
        public List<Question> Questions { get; set; }
        public List<Answer> Answers { get; set; }
    }

    public class SurveyResultViewModel
    {
        public String SurveyTitle { get; set; }
        public List<SurveyResultItemQuestion> Questions { get; set; }
        public int CorrectAmount { get; set; }
    }

    public class SurveyResultItemQuestion
    {
        public String QuestionTitle { get; set; }
        public List<SurveyResultItemAnswer> Answers { get; set; }
    }

    public class SurveyResultItemAnswer
    {
        public String AnswerTitle { get; set; }
        public String UsersChoice { get; set; }
        public bool IsCorrect { get; set; }
    }
    public class Question
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public List<Option> Options { get; set; }
    }

    public class Option
    {
        public string Name { get; set; }
        public bool IsCorrect { get; set; }
        public string Explanation { get; set; }
    }
    public class Response
    {
        public string UserId { get; set; } // user email
        public DateTime DateResponded { get; set; }
        public string Feedback { get; set; }
        public List<Answer> Answers { get; set; }
    }

    public class Answer
    {
        public int QuestionNumber { get; set; }
        public List<Choice> Choices { get; set; }
    }
    public class Choice
    {
        public int OptionNumber { get; set; }
        public string Value { get; set; }
    }
    public class Invitation
    {

    }
}