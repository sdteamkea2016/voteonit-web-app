﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoteOnIt_WebApp.Models
{
    public class Group
    {
        public string Id { get; set; }
        public string GroupName { get; set; }
        public bool? IsPrivate { get; set; }
        public string Code { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}