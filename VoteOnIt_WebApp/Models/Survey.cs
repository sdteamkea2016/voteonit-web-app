﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoteOnIt_WebApp.Models
{
    public class Survey
    {
        public string Id { get; set; }
        public string CreatedById { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public bool IsPrivate { get; set; }
        public List<string> Tags { get; set; }
        public List<Comment> Comments { get; set; }
        public List<Invitation> Invitations { get; set; }
        public List<Question> Questions { get; set; }
        public List<Response> Responses { get; set; }
    }
}