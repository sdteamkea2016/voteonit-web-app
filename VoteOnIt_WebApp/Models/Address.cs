﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoteOnIt_WebApp.Models
{
    public class Address
    {
        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string ZipCode { get; set; }
    }
}