﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoteOnIt_WebApp.Models
{
    public class Role
    {
        public string RoleName { get; set; }
        public string GrantedBy { get; set; }
        public DateTime DateGranted { get; set; }
    }
}