﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VoteOnIt_WebApp.Models;

namespace VoteOnIt_WebApp.Controllers
{
    public class GroupsController : Controller
    {
        // GET: Groups
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            IEnumerable<Group> groups = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Get GetAllGroups
                HttpResponseMessage response = await client.GetAsync(string.Format("api/group"));
                groups = await response.Content.ReadAsAsync<IEnumerable<Group>>();
            }
            return View(groups);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateGroupViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Get cookie values
                string userEmail = User.Identity.Name;

                User currentUser = null;
                HttpResponseMessage response1 = await client.GetAsync(string.Format("api/user/email/{0}/", userEmail));
                currentUser = await response1.Content.ReadAsAsync<User>();

                // HTTP POST CreateGroup
                // Not all arguments are mandatory. Values can be null.
                Group newGroup = new Group();
                newGroup.GroupName = model.GroupName;
                newGroup.IsPrivate = model.IsPrivate;
                newGroup.Code = model.Code;
                newGroup.CreatedBy = currentUser.Id;

                HttpResponseMessage response = await client.PostAsJsonAsync("api/group", newGroup);

                if (response.IsSuccessStatusCode)
                {
                    return Redirect("~/Groups/Index");
                }
                else
                {
                    ModelState.AddModelError("GroupName", "Group name already in use");
                    return View(model);
                }
            }
        }

        [HttpGet]
        public async Task<ActionResult> Edit()
        {
            Group currentGroup = null;
            EditGroupViewModel model = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string url = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                string groupID = url.Split('/').Last();

                // Get Group from id
                HttpResponseMessage response = await client.GetAsync(string.Format("api/group/{0}/", groupID));
                currentGroup = await response.Content.ReadAsAsync<Group>();

            }

            model = new EditGroupViewModel()
            {
                GroupName = currentGroup.GroupName,
                IsPrivate = currentGroup.IsPrivate,
                Code = currentGroup.Code
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditGroupViewModel model)
        {
             if (!ModelState.IsValid)
            {
                return View(model);
            }
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Get cookie values
                string userEmail = User.Identity.Name;
				
				// Get Group ID
				string url = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
				string groupID = url.Split('/').Last();
				
                // Get user from email
                User currentUser = null;
                HttpResponseMessage response = await client.GetAsync(string.Format("api/user/email/{0}/", userEmail));
                currentUser = await response.Content.ReadAsAsync<User>();
				
				// Get Group from id
				Group currentGroup = null;
                HttpResponseMessage response2 = await client.GetAsync(string.Format("api/group/{0}/", groupID));
                currentGroup = await response2.Content.ReadAsAsync<Group>();

                bool updategroup = false;

                // Check if the current user is the owner of the group
                if (currentGroup.CreatedBy == currentUser.Id)
				{
					// Check for any changes in model
					if (model.GroupName != currentGroup.GroupName)
					{
						// GET groupExist
						HttpResponseMessage groupresponse = await client.GetAsync(string.Format("api/group/exists/{0}/", model.GroupName));
						Boolean groupExist = await groupresponse.Content.ReadAsAsync<Boolean>();
						if (groupresponse.IsSuccessStatusCode && groupExist == false)
						{
                            currentGroup.GroupName = model.GroupName;
                            updategroup = true;
                        }
						else
						{
							ModelState.AddModelError("GroupName", "Group name is already in use");
							return View(model);
						}
					}
					if (model.IsPrivate != currentGroup.IsPrivate)
					{
                        currentGroup.IsPrivate = model.IsPrivate;
                        updategroup = true;
                    }
					if (model.Code != currentGroup.Code)
					{
                        currentGroup.Code = model.Code;
                        updategroup = true;
                    }
					// Check for any changes, then updates group
					if (updategroup == true)
					{
						// HTTP Put updateGroup
						// Updates the data for updated user
						HttpResponseMessage response3 = await client.PutAsJsonAsync("api/group", currentGroup);
						if (response3.IsSuccessStatusCode)
						{
                            return Redirect("~/Groups/Index");
                        }
						else
						{
							ModelState.AddModelError("GroupName", "Something went wrong, please try again later");
							return View(model);
						}
					}
				}
				else 
				{
					ModelState.AddModelError("GroupName", "You are not the owner of this group.");
					return View(model);
				}

                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Delete()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Delete(DeleteGroupViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Get cookie values
                string userEmail = User.Identity.Name;
				
				// Get Group ID
				string url = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
				string groupID = url.Split('/').Last();
				
				// Get user from email
				User currentUser = null;
                HttpResponseMessage response = await client.GetAsync(string.Format("api/user/email/{0}/", userEmail));
                currentUser = await response.Content.ReadAsAsync<User>();
				
				// Get Group from id
				Group currentGroup = null;
                HttpResponseMessage response2 = await client.GetAsync(string.Format("api/group/{0}/", groupID));
                currentGroup = await response2.Content.ReadAsAsync<Group>();
				
				if (currentGroup.CreatedBy == currentUser.Id)
				{
					// HTTP GET CheckLogin
					// Check if current password match the email
					HttpResponseMessage response3 = await client.GetAsync(string.Format("api/account/{0}/{1}", userEmail, model.CurrentPassword));
					User loggedUser = await response3.Content.ReadAsAsync<User>();
					if (response3.IsSuccessStatusCode && loggedUser != null)
					{
						// HTTP Delete group
						// check if group was deleted correctly
						HttpResponseMessage response4 = await client.DeleteAsync(string.Format("api/group/{0}", groupID));
						Boolean statusCode = await response4.Content.ReadAsAsync<Boolean>();
						if (response4.IsSuccessStatusCode && statusCode == true)
						{
							return Redirect("~/Groups/Index");
						}
						else
						{
							ModelState.AddModelError("CurrentPassword", "Something went wrong, please try again later");
							return View(model);
						}
					}
					else
					{
						ModelState.AddModelError("CurrentPassword", "Current password did not match");
						return View(model);
					}
				}
				else
				{
					ModelState.AddModelError("CurrentPassword", "You are not owner of this group");
                    return View(model);
				}
            }
        }

        [HttpGet]
        public async Task<ActionResult> Details()
        {
            IEnumerable<User> users = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Get Group ID
                string url = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                string groupID = url.Split('/').Last();

                // Get users from group id
                HttpResponseMessage response = await client.GetAsync(string.Format("api/user/group/{0}/", groupID));
                var content = response.Content.Headers.ContentLength.Value;
                if (response.IsSuccessStatusCode && content > 2)
                {
                    users = await response.Content.ReadAsAsync<IEnumerable<User>>();
                }
                else
                {
                    users = null;
                }
            }
            return View(users);
        }

        [HttpGet]
        public ActionResult Join()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Join(JoinGroupViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Get cookie values
                string userEmail = User.Identity.Name;

                // Get Group ID
                string url = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                string groupID = url.Split('/').Last();

                // Get user from email
                User currentUser = null;
                HttpResponseMessage response = await client.GetAsync(string.Format("api/user/email/{0}/", userEmail));
                currentUser = await response.Content.ReadAsAsync<User>();
                string userId = currentUser.Id;

                // Get Group from id
                Group currentGroup = null;
                HttpResponseMessage response2 = await client.GetAsync(string.Format("api/group/{0}/", groupID));
                currentGroup = await response2.Content.ReadAsAsync<Group>();

                // Check if model code match groups code
                if (model.Code == currentGroup.Code)
                {
                    // Check if user exist in group
                    HttpResponseMessage response3 = await client.GetAsync(string.Format("api/user/group/exists/{0}/{1}/", currentUser.Id, currentGroup.Id));
                    Boolean userExist = await response3.Content.ReadAsAsync<Boolean>();
                    if (response3.IsSuccessStatusCode && userExist == false)
                    {
                        // add user to group
                        var route = "api/group/add/" + groupID + "/" + userId + "/";
                        HttpResponseMessage response4 = await client.PutAsJsonAsync(route, userId);
                        if (response4.IsSuccessStatusCode)
                        {
                            return Redirect("~/Groups/Details/" + groupID);
                        }
                        else
                        {
                            ModelState.AddModelError("Code", "Something went wrong, please try again later");
                            return View(model);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("Code", "You are allready a member of this group");
                        return View(model);
                    }

                }
                else
                {
                    ModelState.AddModelError("Code", "Invitation Code did not match");
                    return View(model);
                }
            }
        }

        [HttpGet]
        public ActionResult Leave()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Leave(LeaveGroupViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Get cookie values
                string userEmail = User.Identity.Name;

                // Get Group ID
                string url = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                string groupID = url.Split('/').Last();

                // Get user from email
                User currentUser = null;
                HttpResponseMessage response = await client.GetAsync(string.Format("api/user/email/{0}/", userEmail));
                currentUser = await response.Content.ReadAsAsync<User>();
                string userId = currentUser.Id;

                // Get Group from id
                Group currentGroup = null;
                HttpResponseMessage response2 = await client.GetAsync(string.Format("api/group/{0}/", groupID));
                currentGroup = await response2.Content.ReadAsAsync<Group>();

                // Check if model code match groups code
                if (model.CurrentPassword == currentUser.Password)
                {
                    // Check if user exist in group
                    HttpResponseMessage response3 = await client.GetAsync(string.Format("api/user/group/exists/{0}/{1}/", currentUser.Id, currentGroup.Id));
                    Boolean userExist = await response3.Content.ReadAsAsync<Boolean>();
                    if (response3.IsSuccessStatusCode && userExist == true)
                    {
                        // remove user from group
                        var route = "api/group/remove/" + groupID + "/" + userId + "/";
                        HttpResponseMessage response4 = await client.PutAsJsonAsync(route, userId);
                        if (response4.IsSuccessStatusCode)
                        {
                            return Redirect("~/Groups/Details/"+groupID);
                        }
                        else
                        {
                            ModelState.AddModelError("CurrentPassword", "Something went wrong, please try again later");
                            return View(model);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("CurrentPassword", "You are not a member of this group");
                        return View(model);
                    }

                }
                else
                {
                    ModelState.AddModelError("CurrentPassword", "Current password does not match");
                    return View(model);
                }
            }
        }

    }
}