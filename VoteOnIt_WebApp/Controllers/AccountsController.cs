﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using VoteOnIt_WebApp.Models;
using System.Threading.Tasks;

namespace VoteOnIt_WebApp.Controllers
{
    public class AccountsController : Controller
    {
        //private UserController _userController = new UserController();

		[HttpGet]
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // HTTP GET CheckLogin
                HttpResponseMessage response = await client.GetAsync(string.Format("api/account/{0}/{1}", model.Email, model.Password));
                User loggedUser = await response.Content.ReadAsAsync<User>();
                if (response.IsSuccessStatusCode && loggedUser != null)
                {
                    // set up authentication
                    FormsAuthentication.SetAuthCookie(model.Email, false);

                    // TODO: set up role and identification for current user

                    //redirect to members, just home for now
                    return Redirect("~/Surveys");
                }
            }

            ModelState.AddModelError("Email", "The credentials does not match");
            return View(model);
        }

        public ActionResult Logout()
          {
              FormsAuthentication.SignOut();
              return Redirect("Index");
          }

		  [HttpGet]
          [AllowAnonymous]
          public ActionResult Register()
          {
			return View();
          }

        [HttpPost]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // HTTP POST AddUser
                // Not all arguments are mandatory. Values can be null.

                User newUser = new User();
                newUser.Username = model.Username;
                newUser.FirstName = model.Firstname;
                newUser.LastName = model.Lastname;
                newUser.Email = model.Email;
                newUser.Gender = model.Gender;
                newUser.Password = model.Password;
                newUser.DateOfBirth = model.DateOfBirth;

                HttpResponseMessage response = await client.PostAsJsonAsync("api/user", newUser);

                if (response.IsSuccessStatusCode)
                {
                    return Redirect("Index"); // redirect to login page
                }
                else
                {
                    ModelState.AddModelError("Email", "Username or Email already in use");
                    return View(model);
                }
            }
        }

        [HttpGet]
        public ActionResult Settings()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> EditDetails()
        {
            User currentUser = null;
            EditDetailsViewModel model = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Get cookie values
                string userEmail = User.Identity.Name;

                HttpResponseMessage response = await client.GetAsync(string.Format("api/user/email/{0}/", userEmail));
                currentUser = await response.Content.ReadAsAsync<User>();

            }

            model = new EditDetailsViewModel()
            {
                Username = currentUser.Username,
                Email = currentUser.Email,
                DateOfBirth = currentUser.DateOfBirth,
                FirstName = currentUser.FirstName,
                LastName = currentUser.LastName,
                Gender = currentUser.Gender
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EditDetails(EditDetailsViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Get cookie values
                string userEmail = User.Identity.Name;

                User currentUser = null;
                User updatedUser = new User();

                // Get current user data from the db
                HttpResponseMessage response1 = await client.GetAsync(string.Format("api/user/email/{0}/", userEmail));
                currentUser = await response1.Content.ReadAsAsync<User>();
                // Check for any changes, then set new values
                if (model.Username != currentUser.Username)
                {
                    // GET userExist
                    HttpResponseMessage userresponse = await client.GetAsync(string.Format("api/user/exists/{0}/", model.Username));
                    Boolean userExist = await userresponse.Content.ReadAsAsync<Boolean>();
                    if (userresponse.IsSuccessStatusCode && userExist == false)
                    {
                        updatedUser.Username = model.Username;
                    }
                    else
                    {
                        ModelState.AddModelError("Username", "Username is allready in use");
                        return View(model);
                    }
                }
                if (model.FirstName != currentUser.FirstName)
                {
                    updatedUser.FirstName = model.FirstName;
                }
                if (model.LastName != currentUser.LastName)
                {
                    updatedUser.LastName = model.LastName;
                }
                if (model.Gender != currentUser.Gender)
                {
                    updatedUser.Gender = model.Gender;
                }
                if (model.Email != currentUser.Email)
                {
                    // GET emailExist
                    HttpResponseMessage emailresponse = await client.GetAsync(string.Format("api/user/email/exists/{0}/", model.Email));
                    Boolean emailExist = await emailresponse.Content.ReadAsAsync<Boolean>();
                    if (emailresponse.IsSuccessStatusCode && emailExist == false)
                    {
                        updatedUser.Email = model.Email;

                        // update authentication
                        FormsAuthentication.SetAuthCookie(model.Email, false);
                    }
                    else
                    {
                        ModelState.AddModelError("Email", "Email is allready in use");
                        return View(model);
                    }
                }
                if (model.DateOfBirth != currentUser.DateOfBirth)
                {
                    updatedUser.DateOfBirth = model.DateOfBirth;
                }

                // Check for any changes, then updates user
                if (model.Username != currentUser.Username || model.FirstName != currentUser.FirstName || model.LastName != currentUser.LastName || model.Gender != currentUser.Gender || model.Email != currentUser.Email || model.DateOfBirth != currentUser.DateOfBirth)
                {
                    updatedUser.Id = currentUser.Id;
                    // HTTP Put Updateuser
                    // Updates the data for updated user
                    HttpResponseMessage response2 = await client.PutAsJsonAsync("api/user", updatedUser);
                    if (response2.IsSuccessStatusCode)
                    {
                        return Redirect("Settings");
                    }
                    else
                    {
                        ModelState.AddModelError("Username", "Something went wrong, please try again later");
                        return View(model);
                    }
                }

                return View(model);
            }
        }

        [HttpGet]
        public ActionResult EditPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> EditPassword(EditPasswordViewModel model)
        {

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Get cookie values
                string userEmail = User.Identity.Name;

                // HTTP GET CheckLogin
                // Check if current password match the email
                HttpResponseMessage response = await client.GetAsync(string.Format("api/account/{0}/{1}", userEmail, model.currentPassword));
                User loggedUser = await response.Content.ReadAsAsync<User>();
                if (response.IsSuccessStatusCode && loggedUser != null)
                {
                    User updatedUser = new User();

                    updatedUser.Id = loggedUser.Id;
                    updatedUser.Password = model.newPassword;

                    // HTTP Put Updateuser
                    // Updates the password for updated user
                    HttpResponseMessage response2 = await client.PutAsJsonAsync("api/user", updatedUser);
                    if (response2.IsSuccessStatusCode)
                    {
                        return Redirect("~/Surveys");
                    }
                    else
                    {
                        ModelState.AddModelError("currentPassword", "Something went wrong, please try again later");
                        return View(model);
                    }
                }
                else
                {
                    ModelState.AddModelError("currentPassword", "Current password did not match");
                    return View(model);
                }
            }
        }

        [HttpGet]
        public ActionResult DeleteAccount()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> DeleteAccount(DeleteAccountViewModel model)
        {

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Get cookie values
                string userEmail = User.Identity.Name;

                // HTTP GET CheckLogin
                // Check if current password match the email
                HttpResponseMessage response = await client.GetAsync(string.Format("api/account/{0}/{1}", userEmail, model.CurrentPassword));
                User loggedUser = await response.Content.ReadAsAsync<User>();
                if (response.IsSuccessStatusCode && loggedUser != null)
                {

                    // HTTP Delete Account
                    // check if user was deleted correctly
                    HttpResponseMessage response2 = await client.DeleteAsync(string.Format("api/user/{0}", loggedUser.Id));
                    Boolean statusCode = await response2.Content.ReadAsAsync<Boolean>();
                    if (response2.IsSuccessStatusCode && statusCode == true)
                    {
                        FormsAuthentication.SignOut();
                        return Redirect("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("CurrentPassword", "Something went wrong, please try again later");
                        return View(model);
                    }
                }
                else
                {
                    ModelState.AddModelError("CurrentPassword", "Current password did not match");
                    return View(model);
                }
            }
        }
    }
}