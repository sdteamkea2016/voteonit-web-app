﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using VoteOnIt_WebApp.Models;

namespace VoteOnIt_WebApp.Controllers
{
    public class SurveysController : Controller
    {

        // GET: Surveys
        public async Task<ActionResult> Index()
        {
            List<SurveyViewModel> list = new List<SurveyViewModel>();
            IEnumerable<Survey> tempList = null;
            var currentUser = User.Identity.Name;
            // Get a list of all surveys
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/survey");
                if (response.IsSuccessStatusCode)
                {
                    tempList = await response.Content.ReadAsAsync<IEnumerable<Survey>>();
                    foreach (var survey in tempList.Where(x=>!x.IsPrivate || x.CreatedById == currentUser))
                    {
                        list.Add(new SurveyViewModel()
                        {
                            Id = survey.Id,
                            CreatedById = survey.CreatedById,
                            Title = survey.Title,
                            Description = survey.Description,
                            DateCreated = survey.DateCreated,
                            ValidFrom = survey.ValidFrom,
                            ValidTo = survey.ValidTo,
                            IsPrivate = survey.IsPrivate,
                            Tags = new List<string>(),
                            Comments = new List<Comment>(),
                            Options = new List<Option>(),
                            Responses = new List<Response>(),
                            Questions = survey.Questions,
                        });
                    }
                }
            }
                return View(list);
        }


        // GET: Surveys by user
        public async Task<ActionResult> MySurveys()
        {
            List<SurveyViewModel> list = new List<SurveyViewModel>();
            IEnumerable<Survey> tempList = null;
            // Get a list of all surveys
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var currentUser = User.Identity.Name;
                // get surveys from user email
                HttpResponseMessage response = await client.GetAsync(string.Format("api/survey/get/{0}/", currentUser));
                var content = response.Content.Headers.ContentLength.Value;
                if (response.IsSuccessStatusCode && content > 2)
                {
                    tempList = await response.Content.ReadAsAsync<IEnumerable<Survey>>();

                    foreach (var survey in tempList)
                    {
                        list.Add(new SurveyViewModel()
                        {
                            Id = survey.Id,
                            CreatedById = survey.CreatedById,
                            Title = survey.Title,
                            Description = survey.Description,
                            DateCreated = survey.DateCreated,
                            ValidFrom = survey.ValidFrom,
                            ValidTo = survey.ValidTo,
                            IsPrivate = survey.IsPrivate,
                            Tags = new List<string>(),
                            Comments = new List<Comment>(),
                            Questions = survey.Questions,
                            Responses = new List<Response>(),
                        });
                    }
                }
                else
                {
                    list = null;
                }
            }
            return View(list);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateSurveyViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // HTTP POST AddSurvey
                // Not all arguments are mandatory. Values can be null.
                Survey newSurvey = new Survey()
                {
                    Title = model.Title,
                    CreatedById = User.Identity.Name,
                    Description = model.Description,
                    DateCreated = DateTime.Now,
                    ValidFrom = model.ValidFrom,
                    ValidTo = model.ValidTo,
                    IsPrivate = model.IsPrivate,
                    Questions = model.Questions
                };
                HttpResponseMessage response = await client.PostAsJsonAsync("api/survey", newSurvey);

                if (response.IsSuccessStatusCode)
                {
                    return Redirect("~/Surveys/MySurveys"); // redirect to my surves list
                }
                return View(model);
            }
        }


        [HttpGet]
        public ActionResult Delete()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Delete(DeleteSurveyViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Get cookie values
                     string userEmail = User.Identity.Name;

                // Get Survey ID
                string url = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                string lastPart = url.Split('/').Last();
                string surveyID = lastPart;

                // Get user from email
                User currentUser = null;
                HttpResponseMessage response1 = await client.GetAsync(string.Format("api/user/email/{0}/", userEmail));
                currentUser = await response1.Content.ReadAsAsync<User>();

                // Get Survey from id
                Survey currentSurvey = null;
                HttpResponseMessage response2 = await client.GetAsync(string.Format("api/survey/{0}/", surveyID));
                currentSurvey = await response2.Content.ReadAsAsync<Survey>();

                if (currentSurvey.CreatedById == currentUser.Email)
                {
                   
                    // HTTP GET CheckLogin
                    // Check if current password match the email
                    HttpResponseMessage response3 = await client.GetAsync(string.Format("api/account/{0}/{1}", userEmail, model.CurrentPassword));
                    User loggedUser = await response3.Content.ReadAsAsync<User>();
                    if (response3.IsSuccessStatusCode && loggedUser != null)
                    {
                        // HTTP Delete survey
                        // check if survey was deleted correctly
                        HttpResponseMessage response4 = await client.DeleteAsync(string.Format("api/survey/{0}", surveyID));
                        Boolean statusCode = await response4.Content.ReadAsAsync<Boolean>();
                        if (response4.IsSuccessStatusCode && statusCode == true)
                        {

                            return Redirect("~/Surveys/MySurveys");
                        }
                        else
                        {

                            ModelState.AddModelError("CurrentPassword", "Something went wrong, please try again later");
                            return View(model);
                        }
                    }
                    else
                    {

                        ModelState.AddModelError("CurrentPassword", "Current password did not match");
                        return View(model);
                    }
                }
                else
                {
                    ModelState.AddModelError("CurrentPassword", "You are not owner of this survey");
                    return View(model);
                }
            }
        }

        [HttpGet]
        public async Task<ActionResult> Edit()
        {
            Survey currentSurvey = null;
            EditSurveyViewModel model = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string url = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                string surveyID = url.Split('/').Last();

                // Get Group from id
                HttpResponseMessage response = await client.GetAsync(string.Format("api/survey/{0}/", surveyID));
                currentSurvey = await response.Content.ReadAsAsync<Survey>();

            }

            model = new EditSurveyViewModel()
            {
                Title = currentSurvey.Title,
                Description = currentSurvey.Description,
                ValidFrom = currentSurvey.ValidFrom,
                ValidTo = currentSurvey.ValidTo,
                IsPrivate = currentSurvey.IsPrivate,
                //Tags = currentSurvey.Tags,
                Questions = currentSurvey.Questions
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditSurveyViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Get cookie values
                string userEmail = User.Identity.Name;

                // Get Group ID
                string url = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                string surveyID = url.Split('/').Last();

                // Get user from email
                User currentUser = null;
                HttpResponseMessage response = await client.GetAsync(string.Format("api/user/email/{0}/", userEmail));
                currentUser = await response.Content.ReadAsAsync<User>();

                // Get Survey from id
                Survey currentSurvey = null;
                HttpResponseMessage response2 = await client.GetAsync(string.Format("api/survey/{0}/", surveyID));
                currentSurvey = await response2.Content.ReadAsAsync<Survey>();

                bool updatesurvey = false;

                // Check if the current user is the owner of the survey
                if (currentSurvey.CreatedById == currentUser.Email)
                {
                    // Check for any changes in model
                    if (model.Title != currentSurvey.Title)
                    {
                        currentSurvey.Title = model.Title;
                        updatesurvey = true;
                    }
                    if (model.Description != currentSurvey.Description)
                    {
                        currentSurvey.Description = model.Description;
                        updatesurvey = true;
                    }
                    if (model.ValidFrom != currentSurvey.ValidFrom)
                    {
                        currentSurvey.ValidFrom = model.ValidFrom;
                        updatesurvey = true;
                    }
                    if (model.ValidTo != currentSurvey.ValidTo)
                    {
                        currentSurvey.ValidTo = model.ValidTo;
                        updatesurvey = true;
                    }
                    //if (model.Tags != currentSurvey.Tags)
                    //{
                    //    currentSurvey.Tags = model.Tags;
                    //      updatesurvey = true;
                    //}
                    //if (model.Questions != currentSurvey.Questions)
                    //{
                        currentSurvey.Questions = model.Questions;
                        //updatesurvey = true;
                    //}
                    // Check for any changes, then updates survey
                    if (updatesurvey == true)
                    {
                        // HTTP Put updateSurvet
                        // Updates the data for updated survey
                        HttpResponseMessage response3 = await client.PutAsJsonAsync("api/survey", currentSurvey);
                        if (response3.IsSuccessStatusCode)
                        {
                            return Redirect("~/Surveys/MySurveys");
                        }
                        else
                        {
                            ModelState.AddModelError("Title", "Something went wrong, please try again later");
                            return View(model);
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("Title", "You are not the owner of this survey.");
                    return View(model);
                }

                return View(model);
            }

        }

        [HttpGet]
        public async Task<ActionResult> StartSurvey()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string url = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                string id = url.Split('/').Last();

                HttpResponseMessage response = await client.GetAsync(string.Format("api/survey/{0}/", id));
                if (response.IsSuccessStatusCode)
                {
                    var survey = await response.Content.ReadAsAsync<Survey>();
                    if (survey != null)
                    {
                        var model = new StartSurveyViewModel()
                        {
                            Id = survey.Id,
                            CreatedById = survey.CreatedById,
                            Title = survey.Title,
                            Description = survey.Description,
                            Questions = survey.Questions,
                        };
                        if (!model.Questions.Any()) { return View("Error"); }
                        return View(model);
                    }
                    return View("Error");
                }
                return View("Error");
            }
        }
        [HttpPost]
        public async Task<ActionResult> StartSurvey(StartSurveyViewModel model)
        {
            Survey survey = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://voteonitws.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage responseSurvey = await client.GetAsync(string.Format("api/survey/{0}/", model.Id));
                
                if (responseSurvey.IsSuccessStatusCode)
                {
                    survey = await responseSurvey.Content.ReadAsAsync<Survey>();
                    survey.Responses.Add(new Response()
                    {
                        UserId = User.Identity.Name,
                        DateResponded = DateTime.Now,
                        Answers = model.Answers,
                    });
                    HttpResponseMessage responseUpdate = await client.PutAsJsonAsync("api/survey", survey);
                    if (responseUpdate.IsSuccessStatusCode)
                    {

                    }
                    TempData["answers"] = model.Answers;
                    TempData["questions"] = survey.Questions;
                    return RedirectToAction("ShowSurveyResults", new RouteValueDictionary(new {title=survey.Title}));
                } 
            }
            return View("Error");
        }

        public ActionResult ShowSurveyResults(string title)
        {
            int rightAnswers = 0;
            List<Answer> answers = (List<Answer>) TempData["answers"];
            List<Question> questions = (List<Question>) TempData["questions"];
            SurveyResultViewModel model = new SurveyResultViewModel() {SurveyTitle = title, Questions = new List<SurveyResultItemQuestion>()};
            foreach (var answer in answers)
            {
                var question = new SurveyResultItemQuestion()
                {
                    QuestionTitle = questions[answer.QuestionNumber].Title,
                    Answers = new List<SurveyResultItemAnswer>(),
                };
                model.Questions.Add(question);
                foreach (var choice in answer.Choices)
                {
                    var answerItem = new SurveyResultItemAnswer()
                    {
                        AnswerTitle = questions[answer.QuestionNumber].Options[choice.OptionNumber].Name,
                        IsCorrect = questions[answer.QuestionNumber].Options[choice.OptionNumber].IsCorrect,
                        UsersChoice = choice.Value,
                    };
                    question.Answers.Add(answerItem);
                    if (answerItem.IsCorrect && answerItem.UsersChoice.Equals("true"))
                    {
                        rightAnswers++;
                    }
                }
            }
            model.CorrectAmount = rightAnswers;

            return View(model);
        }
    }
}