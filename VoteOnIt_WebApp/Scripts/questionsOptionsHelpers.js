﻿function addQuestion(quest) {
    $('#buttonQuestion' + quest).remove();
    var newquest = quest + 1;
    var newquesttext = newquest + 1;
    $('<div id="question' + newquest + '" >' +
        '<h3>Question ' + newquesttext + '</h3>' +
        '<div class="form-group">' +
        '<label class="control-label col-md-2" for="Questions_' + newquest + '__Title">Title</label>' +
        '<div class="col-md-10">' +
        '<input name="Questions[' + newquest + '].Title" class="form-control text-box single-line" id="Questions_' + newquest + '__Title" type="text" value="">' +
        '<span class="field-validation-valid text-danger" data-valmsg-replace="true" data-valmsg-for="Questions[' + newquest + '].Title"></span> </div> </div>' +
        '<div class="form-group">' +
        '<label class="control-label col-md-2" for="Questions_' + newquest + '__Description">Description</label>' +
        '<div class="col-md-10">' +
        '<input name="Questions[' + newquest + '].Description" class="form-control text-box single-line" id="Questions_' + newquest + '__Description" type="text" value="">' +
            '<span class="field-validation-valid text-danger" data-valmsg-replace="true" data-valmsg-for="Questions[' + newquest + '].Description"></span></div></div>' +
        '<div class="form-group">' +
        '<label class="control-label col-md-2" for="Questions_' + newquest + '__Type">Type</label>' +
        '<div class="col-md-10">' +
        '<input name="Questions[' + newquest + '].Type" class="form-control text-box single-line" id="Questions_' + newquest + '__Type" type="text" value="">' +
            '<span class="field-validation-valid text-danger" data-valmsg-replace="true" data-valmsg-for="Questions[' + newquest + '].Type"></span></div></div>' +
        '<div id="optionsForQuest' + newquest + '">' +
        '<div id="option0" class="col-md-offset-2">' +
        '<h4>Option 1</h4>' +
        '<div class="form-group">' +
        '<label class="control-label col-md-2" for="Questions_' + newquest + '__Options_0__Name">Name</label>' +
        '<div class="col-md-10">' +
        '<input name="Questions[' + newquest + '].Options[0].Name" class="form-control text-box single-line" id="Questions_' + newquest + '__Options_0__Name" type="text" value="">' +
            '<span class="field-validation-valid text-danger" data-valmsg-replace="true" data-valmsg-for="Questions[' + newquest + '].Options[0].Name"></span></div></div>' +
        '<div class="form-group">' +
        '<label class="control-label col-md-2" for="Questions_' + newquest + '__Options_0__Explanation">Explanation</label>' +
        '<div class="col-md-10">' +
        '<input name="Questions[' + newquest + '].Options[0].Explanation" class="form-control text-box single-line" id="Questions_' + newquest + '__Options_0__Explanation" type="text" value="">' +
            '<span class="field-validation-valid text-danger" data-valmsg-replace="true" data-valmsg-for="Questions[' + newquest + '].Options[0].Explanation"></span></div></div>' +
        '<div class="form-group">' +
        '<label class="control-label col-md-2" for="Questions_' + newquest + '__Options_0__IsCorrect">IsCorrect</label>' +
        '<div class="col-md-10">' +
        '<input name="Questions[' + newquest + '].Options[0].IsCorrect" class="form-control check-box" id="Questions_' + newquest + '__Options_0__IsCorrect" type="checkbox" value="true" data-val-required="Feltet IsCorrect skal udfyldes." data-val="true">' +
        '<input name="Questions[' + newquest + '].Options[0].IsCorrect" type="hidden" value="false">' +
            '<span class="field-validation-valid text-danger" data-valmsg-replace="true" data-valmsg-for="Questions[' + newquest + '].Options[0].IsCorrect"></span></div></div>' +
        '</div>' +
        '<div id="option1" class="col-md-offset-2"><h4>Option 2</h4>' +
        '<div class="form-group">' +
        '<label class="control-label col-md-2" for="Questions_' + newquest + '__Options_1__Name">Name</label>' +
        '<div class="col-md-10">' +
        '<input name="Questions[' + newquest + '].Options[1].Name" class="form-control text-box single-line" id="Questions_' + newquest + '__Options_1__Name" type="text" value="">' +
            '<span class="field-validation-valid text-danger" data-valmsg-replace="true" data-valmsg-for="Questions[' + newquest + '].Options[1].Name"></span></div></div>' +
        '<div class="form-group">' +
        '<label class="control-label col-md-2" for="Questions_' + newquest + '__Options_1__Explanation">Explanation</label>' +
        '<div class="col-md-10">' +
        '<input name="Questions[' + newquest + '].Options[1].Explanation" class="form-control text-box single-line" id="Questions_' + newquest + '__Options_1__Explanation" type="text" value="">' +
            '<span class="field-validation-valid text-danger" data-valmsg-replace="true" data-valmsg-for="Questions[' + newquest + '].Options[1].Explanation"></span></div></div>' +
        '<div class="form-group">' +
        '<label class="control-label col-md-2" for="Questions_' + newquest + '__Options_1__IsCorrect">IsCorrect</label>' +
        '<div class="col-md-10">' +
        '<input name="Questions[' + newquest + '].Options[1].IsCorrect" class="form-control check-box" id="Questions_' + newquest + '__Options_1__IsCorrect" type="checkbox" value="true" data-val-required="Feltet IsCorrect skal udfyldes." data-val="true">' +
        '<input name="Questions[' + newquest + '].Options[1].IsCorrect" type="hidden" value="false">' +
            '<span class="field-validation-valid text-danger" data-valmsg-replace="true" data-valmsg-for="Questions[' + newquest + '].Options[1].IsCorrect"></span></div></div>' +
        '<input type="button" onclick="addOption(' + newquest + ', 1)" id="buttonQuestion' + newquest + 'Option1" value="Add Option" class="btn btn-default"/></div>' +
        '</div><br/>' +
        '<input type="button" onclick="addQuestion(' + newquest + ')" id="buttonQuestion' + newquest + '" value="Add Question" class="btn btn-default"/>' +
        '&nbsp;<input type="button" onclick="deleteQuestion(' + newquest + ')" id="buttonQuestion' + newquest + 'Delete" value="Delete Question" class="btn btn-default" />' +
        '</div>').insertAfter(".questions #question" + quest);
}
function addOption(quest, option) {
    $('#buttonQuestion' + quest + 'Option' + option).remove();
    var newoption = option + 1;
    var newoptiontext = newoption + 1;
    $('<div id="option' + newoption + '" class="col-md-offset-2"><h4>Option ' + newoptiontext + '</h4>' +
        '<div class="form-group">' +
        '<label class="control-label col-md-2" for="Questions_' + quest + '__Options_' + newoption + '__Name">Name</label>' +
        '<div class="col-md-10">' +
        '<input name="Questions[' + quest + '].Options[' + newoption + '].Name" class="form-control text-box single-line" id="Questions_' + quest + '__Options_' + newoption + '__Name" type="text" value="">' +
        '<span class="field-validation-valid text-danger" data-valmsg-replace="true" data-valmsg-for="Questions[' + quest + '].Options[' + newoption + '].Name"></span></div></div>' +
        '<div class="form-group">' +
        '<label class="control-label col-md-2" for="Questions_' + quest + '__Options_' + newoption + '__Explanation">Explanation</label>' +
        '<div class="col-md-10">' +
        '<input name="Questions[' + quest + '].Options[' + newoption + '].Explanation" class="form-control text-box single-line" id="Questions_' + quest + '__Options_' + newoption + '__Explanation" type="text" value="">' +
        '<span class="field-validation-valid text-danger" data-valmsg-replace="true" data-valmsg-for="Questions[' + quest + '].Options[' + newoption + '].Explanation"></span></div></div>' +
        '<div class="form-group">' +
        '<label class="control-label col-md-2" for="Questions_' + quest + '__Options_' + newoption + '__IsCorrect">IsCorrect</label>' +
        '<div class="col-md-10">' +
        '<input name="Questions[' + quest + '].Options[' + newoption + '].IsCorrect" class="form-control check-box" id="Questions_' + quest + '__Options_' + newoption + '__IsCorrect" type="checkbox" value="true" data-val-required="Feltet IsCorrect skal udfyldes." data-val="true">' +
        '<input name="Questions[' + quest + '].Options[' + newoption + '].IsCorrect" type="hidden" value="false">' +
            '<span class="field-validation-valid text-danger" data-valmsg-replace="true" data-valmsg-for="Questions[' + quest + '].Options[' + newoption + '].IsCorrect"></span></div></div>' +
        '<input type="button" onclick="addOption(' + quest + ', ' + newoption + ')" id="buttonQuestion' + quest + 'Option' + newoption + '" value="Add Option" class="btn btn-default"/>' +
        '&nbsp;<input type="button" onclick="deleteOption(' + quest + ', ' + newoption + ')" id="buttonQuestion0Option1Delete" value="Delete Option" class="btn btn-default"/>' +
        '</div>').appendTo("#optionsForQuest" + quest);
}
function deleteOption(quest, option) {
    if (option == 2) {
        var oloption = option - 1;
        $('<input type="button" onclick="addOption(' + quest + ', ' + oloption + ')" id="buttonQuestion' + quest + 'Option' + oloption + '" value="Add Option" class="btn btn-default"/>').appendTo('#option' + oloption);
    }
    $('#optionsForQuest' + quest + ' #option' + option).remove();
}
function deleteQuestion(quest) {
    if (quest == 1) {
        var olquest = quest - 1;
        $('<input type="button" onclick="addQuestion(' + olquest + ')" id="buttonQuestion' + olquest + '" value="Add Question" class="btn btn-default"/>').appendTo('#question'+olquest);
    }
    $('#question' + quest).remove();
}